# switch

We include a simple push button switch on our project board as a generic and adaptable input device. Buttons are simple contact devices that can be used as a mechanics input. The can be used for a wide range of applications such as a simple toggle switch, or for more complex inputs like double pressing or long press inputs to trigger different events.

Connecting such a switch to a microcontroller is as simple as connecting either to the GND pin or 3.3V pin depending if you are active low or high, and then connecting the other side to one of the microcontroller pins that can handle digital/analogue input.

An example wiring to the ESP32 module is below:

## schematic

![ mx switch](pics/mx_bb.png)

An example coding with buttons can be found here https://esp32io.com/tutorials/esp32-button?utm_content=cmp-true
