# neopixel

The Neopixel or more widely known as individually addressable LEDS come in a variety of formats, from single LED, to strips through to more exotic arrangements. These LED modules are very useful since you have the ability to individually control the colour and brightness of a single LED within a whole strip or array from a single MCU pin.

In this project we are using this particular module:

Front of the board

![neopixel-front](pics/led-board-front.png)

Back of the board

![neopixel-back](pics/led-board-back.png)

The Neopixel LED module and strips use a single pin, time dependant UART protocol so wiring this module to your microcontroller is very simple. It is recommended to lookup and use one of the digitial pins of your microcontroller if you are not sure. An example of doing this with the ESP32 microcontroller:

| NEOPIXEL |  ESP32    |
| -------- | --------- |
| 5V/VCC   |  5V       |
| GND      |  GND      |
| D        |  GPIO 16  |


## schematic
![neopixel](pics/rgb_bb.png)

For a more in-depth technical treatment of these LEDs, see this link https://lastminuteengineers.com/ws2812b-arduino-tutorial/
