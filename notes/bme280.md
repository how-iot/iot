# BME 280 board

The BME280 sensor module measures barometric pressure, temperature and humidity. It is commonly used by hobbyists in projects such as room thermostats and weather stations projects. It is generally considered one of the better options for hobbyists as it is fairly accurate, easy to calibrate and is readily available for a reasonable price.

We are using the following board:

Front of the board

![bme280-front](pics/bme-board-front.png)

Back of the board

![bme280-back](pics/bme-board-back.png)

The BME280 sensor module used the I2C communication protocol so wiring this module to your microcontroller is very simple. It is recommended to lookup and use the default I2C pins on your microcontroller if you are not sure. An example of doing this with the ESP32 microcontroller:

| BME280   |  ESP32    |
| -------- | --------- |
| Vin      |  5V       |
| GND      |  GND      |
| SCL      |  GPIO 22  |
| SDA      |  GPIO 21  |

A simple wiring schematic showing this is below.

![bme280](pics/bme280_bb.png)

More advanced technical information as well as software libraries can be found at the following link https://lastminuteengineers.com/bme280-arduino-tutorial/
