
simple multisensor board
========================

this is for pure educational purposes...

notes and more ![qr](pics/how.svg)

pcb

  ![pcb](pics/pcb.png)

board 3d model

  ![pcb](pics/board-3dview.png)

board 3d model no parts

  ![pcb](pics/board-3dview-no.png)

board 3d model back side

  ![pcb](pics/board-3dview-back.png)


bom:

- A1: esp32 devkit 38 pin, type c, https://www.aliexpress.com/item/1005004424644043.html
- U1: 1.8 inch tft spi, https://www.aliexpress.com/item/1005004259333504.html
- U2: rcwl 0516, microwave sensor, https://www.aliexpress.com/item/1005004653220516.html
- U3: bme280 5V, temperature, pressure and humidity sensor, https://www.aliexpress.com/item/32862421810.html
- U4: gy-521 mpu-6050, accelerometer 6dof, https://www.aliexpress.com/item/1536997165.html
- U5: sgp30 air quality sensor, TVOC/eco2 sensor, https://www.aliexpress.com/item/1005004570632106.html
- U6: sgp40 air quality sensor VOC/gas, https://www.aliexpress.com/item/1005004570632106.html
- sw1: cherry switch pcb mount, any
- D1: rgb addresable led, onepixel 5050, 5V, https://www.aliexpress.com/item/1005003208775598.html
- R1: a resistor between 10-100 Ω
icons are mdi icons, converted to footprints

for box, lego 1xYY plates(2,4,6,8) or bricks if underglow light is to be added, to cover 52 studs and 2x 6x16 plate (ID:
6173053/3027 on lego.com)
